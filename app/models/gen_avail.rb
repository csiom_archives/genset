class GenAvail < ActiveRecord::Base

  validates_presence_of :gen_detail_id
  belongs_to :gen_detail
  protokoll :genID, :pattern => "G%y###"

end

class ThirdPartyGen < ActiveRecord::Base
  
  belongs_to :third_party_provider
  belongs_to :gen_detail
  protokoll :genID, :pattern => "TP###"

end

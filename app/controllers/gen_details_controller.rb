class GenDetailsController < ApplicationController
  before_action :set_gen_detail, only: [:show, :edit, :update, :destroy]

  # GET /gen_details
  # GET /gen_details.json
  def index
    @gen_details = GenDetail.all
    @gen_avails = GenAvail.all 
    @notice = params[:notice]
  end

  # GET /gen_details/1
  # GET /gen_details/1.json
  def show
  end

  # GET /gen_details/new
  def new
    @gen_detail = GenDetail.new
  end

  # GET /gen_details/1/edit
  def edit
    @gen_avail = GenAvail.where(gen_detail_id: @gen_detail.id) 
  end

  # POST /gen_details
  # POST /gen_details.json
  def create
    @gen_detail = GenDetail.new(gen_detail_params)

    respond_to do |format|
      if @gen_detail.save

       (1..@gen_detail.totalGen).each do |i|
          @gen_avail = GenAvail.new
          @gen_avail.available = true
          @gen_avail.gen_detail_id = @gen_detail.id
          @gen_avail.save
        end 

        format.html { redirect_to :action => "index"}
      else
        format.html { render :new }
        format.json { render json: @gen_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gen_details/1
  # PATCH/PUT /gen_details/1.json
  def update
    respond_to do |format|
      if @gen_detail.update(gen_detail_params)
        format.html { redirect_to action: "index", notice: "Generator Type is updated successfully."}
      else
        format.html { render :edit }
        format.json { render json: @gen_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gen_details/1
  # DELETE /gen_details/1.json
  def destroy
    @gen_avail = GenAvail.where(gen_detail_id: @gen_detail.id)
    if @gen_detail.destroy
      @gen_avail.each do |g|
        g.destroy
      end
    end
    respond_to do |format|
      format.html { redirect_to action: "index", notice: 'Gen detail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gen_detail
      @gen_detail = GenDetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gen_detail_params
      params.require(:gen_detail).permit(:genType, :genRent, :totalGen)
    end

end

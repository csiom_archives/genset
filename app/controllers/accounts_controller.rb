class AccountsController < ApplicationController
  def index
		@accounts = Account.all
  end

  def show
  end

  def new
		@account = Account.new
  end
	def create
		@account = Account.new(new_acc_params)
		respond_to do |format|
			if @account.save
        format.html { redirect_to controller: "accounts", action: "index", notice: 'Account was successfully added.' }
			end
		end
	end


  def edit
  end
	private
	def new_acc_params
    params.require(:account).permit(:name, :number, :ifsc, :micr)
	end

end
